/**
 * Returns true if login succeded otherwise false
 */
export async function login (username: string, password: string): Promise<boolean> {
	const response = await fetch(`${API_BASE_URL}/oauth/login`, {
		method: 'POST',
		headers: {'Content-Type': 'application/json'},
		body: JSON.stringify({
			username,
			password
		})
	})
	if (!response.status || response.status >= 400) {
		if (response.status === HTTP_UNAUTHORIZED) {
			return false // login failed
		}
		// Some other error
		throw new Error(`oauth login failed (${response.status}): ${response.statusText}`)
	}
	const data = await response.json() as AuthResponse
	refreshToken = data.refresh_token
	accessToken = data.access_token
	// expire our token just a bit before actual expire time
	accessTokenExpire = Date.now() + (data.expires_in * 1000) - 2000
	return true // login succeeded
}

/** Logout user */
export async function logout() {
	accessToken = refreshToken = undefined
	accessTokenExpire = 0
}

/**
 * Fetch wrapper that adds accessToken to header and auto-refreshes the accessToken.
 * Also performs JSON parsing on response text (if there is any)
 */
export async function doFetch<T = void>(url: string, opts: RequestInit = {}): Promise<T> {
	let token = await getAccessToken()
	opts = {...opts}
	const headers: any = {...opts.headers}
	// Add the auth header
	headers.Authorization = 'Bearer ' + token
	// Add content type
	if (opts.body != null) {
		headers['Content-type'] = 'application/json'
	}
	opts.headers = headers
	// Perform the request...
	let response = await fetch(`${API_BASE_URL}/api${url}`, opts)
	if (response.status === HTTP_FORBIDDEN) {
		// Unauthorized - try getting a new access token
		token = await refreshAccessToken()
		;(opts.headers as any).Authorization = 'Bearer ' + token
		response = await fetch(`${API_BASE_URL}/api${url}`, opts)
	}
	if (!response.status || response.status >= 400) {
		// Got a bad status value - throw an error and include the
		// status code in the error object
		const error: any = new Error(response.statusText || 'An error occurred')
		error.status = response.status
		throw error
	}
	// Everything seemed ok. Check if we got response body...
	const resContentType = response.headers.get("content-type")
	if (!resContentType || !resContentType.toLowerCase().includes('application/json')) {
		// Nothing to parse
		return undefined as any as T
	}
	// Assume JSON body - parse and return it
	return await response.json() as T
}

///////////////////////////////////////////////////////////////////////
// Internally used stuff follows...

/** Login and access_token response type */
interface AuthResponse {
	username: string
	roles: string[]
	token_type: string,
	access_token: string
	expires_in: number
	refresh_token: string
}

const API_BASE_URL = 'https://some.url'

// Specific HTTP status codes we look for
const HTTP_UNAUTHORIZED = 401
const HTTP_FORBIDDEN    = 403

// Cache these
// Could also store in local/sessionStorage
let refreshToken: string | undefined
let accessToken: string | undefined
let accessTokenExpire = 0

/**
 * Internally used helper - get a new accessToken.
 * Must be logged in (i.e. we must already have a refreshToken.)
 */
async function refreshAccessToken(): Promise<string> {
	if (!refreshToken) {
		throw new Error("No refresh token (not logged in)")
	}
	const response = await fetch(`${API_BASE_URL}/oauth/access_token`, {
		method: 'POST',
		headers: {'Content-Type': 'application/x-www-form-urlencoded'},
		body: `grant_type=refresh_token&refresh_token=${encodeURIComponent(refreshToken)}`
	})
	if (!response.status || response.status >= 400) {
		throw new Error(`oauth access_token failed (${response.status}): ${response.statusText}`)
	}
	const data = await response.json() as AuthResponse
	refreshToken = data.refresh_token
	accessToken = data.access_token
	// expire our token just a bit before actual expire time
	accessTokenExpire = Date.now() + (data.expires_in * 1000) - 2000
	return accessToken
}

/** Internal helper. Returns existing token or fetches a new one. */
async function getAccessToken() {
	if (accessToken && accessTokenExpire > Date.now()) {
		return accessToken
	}
	return refreshAccessToken()
}
