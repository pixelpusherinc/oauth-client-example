# OAuth Client-Side example

This gist shows how to authenticate and store a refresh token and access token.

First perform login, and from then on use the `doFetch` wrapper which will automatically
send an access token in request headers and auto-refresh that token if it expires.

See the `example-usage.ts` file for example use of the helper module functions.

Some changes will be necessary for the specifics of your own project.

If you need to support older browsers, you'll need to add a [Promise polyfill](https://github.com/taylorhakes/promise-polyfill) and a [fetch polyfill](https://github.com/github/fetch).
