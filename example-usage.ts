import {login, doFetch} from './oauth-helpers'

// High-level API wrapper functions that take care of
// constructing URLs and post data

// Perform initial login
export async function handleLogin (username: string, password: string) {
	const loginOk = await login(username, password)
	if (loginOk) {
		// Login succeeded...
	} else {
		// Login failed...
	}
}

// After logged in can perform authenticated requests like these...

export interface Item {
	id: number
	name: string
}

/** Get a list of Items */
export async function getItems() {
	return doFetch<Item[]>('/items')
}

/** Add a new item, returns new Item ID */
export async function saveItem (item: Item) {
	const result = await doFetch<{id: number}>('/item', {
		method: 'POST',
		body: JSON.stringify(item)
	})
	return result.id
}
